README.txt

Introduction
-------------
The Employee Reimbursement Application provides an interface for both managers and employees of a bank handle interactions with employee reimbursement requests. 

Configuration
--------------
In the pom.xml file you will need to ensure you have the following dependencies.
ojdc10 version 19.8.0.0
junit version 4.12
log4j version 1.2.17
jackson core version 2.11.2
java servlet version 2.5
apache tomcat version 7.0.42

DB username: PROJ1
DB password: P4SSV00RD

Demo
-----
This application has been created with built-in users. There is no option to create new users in this version of the application so it is necessary to know the usernames and passwords of the built-in users

Managers
-username : BSTINSON
-password : LEGENDARY

Customers
-username : BIGFUDGE
-password : LAWYERED

-username : TSCHMOSBY
-password : DR.X

