package dev.pranav.actions;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class VerificationTest {

	Verification v1 = new Verification();
	
	@Test
	public void testWithNullUserName() {
		boolean expected = false;
		boolean actual = v1.verifyUser(null, "asdfj;k");
		assertEquals(expected, actual);
	}
	
	@Test
	public void testWithIncorrectUserName() {
		boolean expected = false;
		boolean actual = v1.verifyUser("man", "incorrect");
		assertEquals(expected, actual);
	}
	
	@Test
	public void testWithNullPasswordAndCorrectUserName() {
		boolean expected = false;
		boolean actual = v1.verifyUser("BSTINSON", null);
		assertEquals(expected, actual);
	}
}
