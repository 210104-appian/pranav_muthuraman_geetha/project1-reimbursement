package dev.pranav.models;

import java.util.ArrayList;
import java.util.List;

public class Manager {
	private int id;
	private String name;
	private String username;
	private String password;
	private List<Request> pendingRequests = new ArrayList<>();
	public Manager() {
		// TODO Auto-generated constructor stub
	}
	
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}


	public List<Request> getPendingRequests() {
		return pendingRequests;
	}


	public void setPendingRequests(List<Request> pendingRequests) {
		this.pendingRequests = pendingRequests;
	}

}
