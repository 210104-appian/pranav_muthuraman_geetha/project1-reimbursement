package dev.pranav.models;

import java.util.ArrayList;
import java.util.List;

public class Employee extends User {	
	private List<Request> requests = new ArrayList<>();
	
	public Employee() {
		// TODO Auto-generated constructor stub
	}

	public List<Request> getRequests() {
		return requests;
	}

	public void setRequests(List<Request> requests) {
		this.requests = requests;
	}
	
}
