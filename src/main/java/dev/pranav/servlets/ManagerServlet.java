package dev.pranav.servlets;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.fasterxml.jackson.databind.ObjectMapper;

import dev.pranav.daos.UserDao;
import dev.pranav.daos.UserDaoImpl;
import dev.pranav.models.Employee;
import dev.pranav.models.Manager;
import dev.pranav.models.Request;

public class ManagerServlet extends HttpServlet{
	private UserDao u1 = new UserDaoImpl();
	private ObjectMapper om = new ObjectMapper();
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException{
		String uri = request.getRequestURI();
//		System.out.println(uri);
		String idstr = uri.split("/")[3];
//		System.out.println(idstr);
		
		Manager m1 = u1.getManager(idstr);
		
		if(m1 == null) {
			System.out.println("it's not getting the managers");
			response.sendError(404);
		}else {	
			m1.setPendingRequests(u1.addPendingRequests());
			try(PrintWriter pw = response.getWriter()){
				pw.write(om.writeValueAsString(m1));
			}
		
		}
	}
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		ObjectMapper om = new ObjectMapper();
		Request r1 = om.readValue(request.getReader().readLine(), Request.class);
		
		if(!u1.updateStatus(r1.getId(), r1.getStatus())) {
			response.sendError(503);
		}else {
			response.setStatus(201);
		}
	}
	
	
}
