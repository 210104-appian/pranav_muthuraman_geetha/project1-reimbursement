package dev.pranav.servlets;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.fasterxml.jackson.databind.ObjectMapper;

import dev.pranav.daos.UserDao;
import dev.pranav.daos.UserDaoImpl;
import dev.pranav.models.Employee;
import dev.pranav.models.Request;

public class EmployeeServlet extends HttpServlet{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private UserDao u1 = new UserDaoImpl();
	private ObjectMapper om = new ObjectMapper();
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException{
		String uri = request.getRequestURI();
//		System.out.println(uri);
		String idstr = uri.split("/")[3];
//		System.out.println(idstr);
		
		Employee e1 = u1.getEmployee(idstr);
		
		if(e1 == null) {
			response.sendError(404);
		}else {		
			e1.setRequests(u1.addEmployeeRequests(e1.getId()));//adds the requests using the id from the employee
			try(PrintWriter pw = response.getWriter()){
				pw.write(om.writeValueAsString(e1));
			}
		
		}
	}
	
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		ObjectMapper om = new ObjectMapper();
		Request r1 = om.readValue(request.getReader().readLine(), Request.class);
		int newID = u1.maxID();
		if(!u1.addNewRequest(newID, r1.getEmployeeID(), r1.getReason(), r1.getAmount())) {
			response.sendError(503);
		}else {
			response.setStatus(201);
		}
	}
	
	
}
