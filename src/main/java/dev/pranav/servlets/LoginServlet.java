package dev.pranav.servlets;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.fasterxml.jackson.databind.ObjectMapper;

import dev.pranav.actions.Verification;
import dev.pranav.models.User;
import dev.pranav.util.*;

public class LoginServlet extends HttpServlet{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private Verification v1 = new Verification();

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		ObjectMapper om = new ObjectMapper();
		User creds = om.readValue(request.getReader().readLine(), User.class);
		
		if(!v1.verifyUser(creds.getUsername(), creds.getPassword())){
			System.out.println("Not an actual user");
			response.sendError(401);//handle the error in the js file
		}else {
			if(v1.isEmployee()) {
				System.out.println("Employee logged in!");
				String token = "E:" + creds.getUsername();
				try(PrintWriter pw = response.getWriter()){
					pw.write(token);
				}
			}else if(v1.isManager()) {
				System.out.println("Manager logged in!");
				String token = "M:" + creds.getUsername();
				try(PrintWriter pw = response.getWriter()){
					pw.write(token);
				}
			}
		}
		
//		BufferedReader bw = request.getReader();
//		String name = bw.readLine();
//		System.out.println(name);
		System.out.println("this was a post request");
		
	}
	
	

}
