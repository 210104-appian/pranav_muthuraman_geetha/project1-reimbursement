package dev.pranav.actions;

import dev.pranav.daos.*;

public class Verification {

	UserDao user = new UserDaoImpl();
	private boolean isEmployee = false;//just need to make sure a new object of this class
	private boolean isManager = false;//is created for every verification (which it should be)
	
	public Verification() {
		// TODO Auto-generated constructor stub
	}

	public boolean verifyUser(String username, String password) {
		if(username == null || password == null) {
			return false;
		}
		String possibleEmployeePassword = user.getEmployeePassword(username);
		String possibleManagerPassword = user.getManagerPassword(username);
		if(possibleEmployeePassword == null && possibleManagerPassword == null)
		{
//			System.out.println("User doesn't exist");
			return false;
		}
		else if(possibleManagerPassword == null) {
			if(password.equals(possibleEmployeePassword)) {
//				System.out.println("logged in");
				setEmployee(true);
				return true;
			}
			else {
//				System.out.println("Employee exists but password's incorrect");
				return false;
			}
		}else {
			if(password.equals(possibleManagerPassword)) {
//				System.out.println("Manager logged in");
				setManager(true);
				return true;
			}else {
//				System.out.println("Manager exists but password's incorrect");
				return false;
			}
		}
	}

	public boolean isManager() {
		return isManager;
	}

	public void setManager(boolean isManager) {
		this.isManager = isManager;
	}

	public boolean isEmployee() {
		return isEmployee;
	}

	public void setEmployee(boolean isEmployee) {
		this.isEmployee = isEmployee;
	}
	
}
