package dev.pranav.daos;

import java.util.List;

import dev.pranav.models.*;

public interface UserDao {
	public String getEmployeePassword(String username);
	public String getManagerPassword(String username);
	public Employee getEmployee(String username);
	public Manager getManager(String username);
	public List <Request> addEmployeeRequests(int id);
	public int maxID();
	public boolean addNewRequest(int id, int employeeID, String reason, double amount);
	public List <Request> addPendingRequests();
	public boolean updateStatus(int id, String status);
}
