package dev.pranav.daos;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import dev.pranav.models.Employee;
import dev.pranav.models.Manager;
import dev.pranav.models.Request;
import dev.pranav.util.ConnectionUtil;

public class UserDaoImpl implements UserDao {

	@Override
	public String getEmployeePassword(String username) {
		String sql = "SELECT PASSWORD FROM EMPLOYEE WHERE USERNAME = ?";
		String password = new String();
		try (Connection connection = ConnectionUtil.getConnection();
				PreparedStatement pStatement = connection.prepareStatement(sql);){
					pStatement.setString(1, username);
					ResultSet resultSet = pStatement.executeQuery();
					if(!resultSet.next()) {
						return null;
					}
					password = resultSet.getString("PASSWORD");//Works
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return password;
	}

	@Override
	public String getManagerPassword(String username) {
		String sql = "SELECT PASSWORD FROM MANAGER WHERE USERNAME = ?";
		String password = new String();
		try (Connection connection = ConnectionUtil.getConnection();
				PreparedStatement pStatement = connection.prepareStatement(sql);){
					pStatement.setString(1, username);
					ResultSet resultSet = pStatement.executeQuery();
					if(!resultSet.next()) {
						return null;
					}
					password = resultSet.getString("PASSWORD");//Works
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return password;
	}

	@Override
	public Employee getEmployee(String username) {
		String sql = "SELECT * FROM EMPLOYEE WHERE USERNAME = ?";
		Employee e1 = new Employee();
		try (Connection connection = ConnectionUtil.getConnection();
				PreparedStatement pStatement = connection.prepareStatement(sql);){
			pStatement.setString(1, username);
			ResultSet resultSet = pStatement.executeQuery();
			
			resultSet.next();
			e1.setId(resultSet.getInt("ID"));
			e1.setName(resultSet.getNString("NAME"));
			e1.setUsername(resultSet.getNString("USERNAME"));
			e1.setPassword(resultSet.getNString("PASSWORD"));
			//***add list of requests***
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return e1;
	}

	@Override
	public Manager getManager(String username) {
		String sql = "SELECT * FROM MANAGER WHERE USERNAME = ?";
		Manager m1 = new Manager();
		try (Connection connection = ConnectionUtil.getConnection();
				PreparedStatement pStatement = connection.prepareStatement(sql);){
			pStatement.setString(1, username);
			ResultSet resultSet = pStatement.executeQuery();
			
			resultSet.next();
			m1.setId(resultSet.getInt("ID"));
			m1.setName(resultSet.getNString("NAME"));
			m1.setUsername(resultSet.getNString("USERNAME"));
			m1.setPassword(resultSet.getNString("PASSWORD"));
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return m1;
	}

	@Override
	public List<Request> addEmployeeRequests(int id) {
		List<Request> requests= new ArrayList<>();
		String sql = "SELECT * FROM REQUEST WHERE EMPLOYEE_ID = ? ORDER BY ID ASC";
		try (Connection connection = ConnectionUtil.getConnection();
				PreparedStatement pStatement = connection.prepareStatement(sql);){
			pStatement.setInt(1, id);
			ResultSet resultSet = pStatement.executeQuery();
			while(resultSet.next()) {
				Request r1 = new Request();
				r1.setId(resultSet.getInt("ID"));
				r1.setStatus(resultSet.getNString("STATUS"));
				r1.setReason(resultSet.getNString("REASON"));
				r1.setAmount(resultSet.getDouble("AMOUNT"));
				requests.add(r1);
			}
			return requests;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}	
		return null;
	}

	@Override
	public int maxID() {
		int index = 1;
		try(Connection connection = ConnectionUtil.getConnection();
				Statement statement = connection.createStatement()){
			ResultSet resultSet = statement.executeQuery("SELECT MAX(ID) FROM REQUEST");
			if (!resultSet.next()) {
				index = 1;
				return index;
			}
			else {
				index = resultSet.getInt(1) + 1;
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return index;
	}

	@Override
	public boolean addNewRequest(int id, int employeeID, String reason, double amount) {
		String sql = "INSERT INTO REQUEST VALUES ( ?, ?, 'PENDING', ?, ?) ";
		try(Connection connection = ConnectionUtil.getConnection();
				PreparedStatement pStatement = connection.prepareStatement(sql);){
			pStatement.setInt(1, id);
			pStatement.setDouble(2, amount);
			pStatement.setInt(3, employeeID);
			pStatement.setString(4, reason);
			pStatement.execute();
			return true;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return false;
	}

	@Override
	public List<Request> addPendingRequests() {
		List<Request> requests= new ArrayList<>();
		String sql = "SELECT * FROM REQUEST WHERE STATUS = 'PENDING' ORDER BY ID ASC";
		try (Connection connection = ConnectionUtil.getConnection();
				PreparedStatement pStatement = connection.prepareStatement(sql);){
			ResultSet resultSet = pStatement.executeQuery();
			while(resultSet.next()) {
				Request r1 = new Request();
				r1.setId(resultSet.getInt("ID"));
				r1.setStatus(resultSet.getNString("STATUS"));
				r1.setReason(resultSet.getNString("REASON"));
				r1.setAmount(resultSet.getDouble("AMOUNT"));
				requests.add(r1);
			}
			return requests;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}	
		return null;
	}

	@Override
	public boolean updateStatus(int id, String status) {
		String sql = "UPDATE REQUEST SET STATUS = ? WHERE ID = ?";
		try(Connection connection = ConnectionUtil.getConnection();
				PreparedStatement pStatement = connection.prepareStatement(sql)) {
			pStatement.setString(1, status);
			pStatement.setInt(2, id);
			pStatement.execute();
			return true;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return false;
	}
	

	
}
