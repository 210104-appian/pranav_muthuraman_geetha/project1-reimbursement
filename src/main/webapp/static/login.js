let employee;

document.getElementById("login-button").onclick = function(){
    let username = document.getElementById("username-input").value;
    let password = document.getElementById("password-input").value;
    // let credentials = (username, password);

    if(username == null || username == ""){
        alert("Please enter your username");
        // document.getElementById("inputs-block").removeChild("username-input");
        // let invalidUserName = document.createElement("input");
        // invalidUserName.type = "text";
        return false;
    }else if (password == null || password == ""){
        alert("Please enter your password");
        return false;
    }
    let credentials = {username, password};
    // let jsonCreds = JSON.stringify(credentials);
    // console.log(jsonCreds);
    performAjaxPostRequest(loginURL, JSON.stringify(credentials), handleSuccessfulLogin, handleUnsuccessfulLogin)

}

function handleSuccessfulLogin(responseText){
    console.log("login successful");
    document.getElementById("failed-msg").hidden = true;
    // sessionStorage.setItem("token", responseText);
    //need to check whether its an employee or manager then redirect might need a get request in here
    if(responseText[0] == "E"){
        sessionStorage.setItem("token", responseText.substring(2));
        window.location.href= "EmployeeHomePage.html";
    }else{
        sessionStorage.setItem("token", responseText.substring(2));
        window.location.href="ManagerHomePage.html";
    }    
}

function handleUnsuccessfulLogin(){
    console.log("login unsuccessful");
    document.getElementById("failed-msg").hidden = false;
}



// let employee = {
//     name: String,
//     username: String,
//     password: String,
//     id: Number,
// };

// let user = document.getElementById("username-input").value;
// console.log(user);