let manager;
let pendingViewed = false;

window.onload = function(){
    managerURI = managerURL + "/" + sessionStorage.getItem("token");
    console.log("load has started");
    console.log(manager);
    performAjaxGetRequest(managerURI, loadManagerData)
}

function loadManagerData(responseText){
    console.log("load completed, check server console");
    console.log(responseText);
    manager = JSON.parse(responseText);
    console.log(manager);
    let welcome = document.createElement("h2");
    welcome.style = "text-align: center;"
    welcome.innerText = "Welcome " + manager.name;
    document.getElementById("welcome-msg").appendChild(welcome);
    return manager;
}

let PRequests = document.getElementById("pending-requests-btn");

PRequests.onclick = function(){
    if(pendingViewed == true){
        return false;
    }
    let reqList = document.createElement("ul");
    let pendingHeader = document.createElement("ol");
    pendingHeader.innerText = "ID   Amount   Reason";
    document.getElementById("pending-requests").appendChild(reqList);
    document.getElementById("pending-requests").appendChild(pendingHeader);
    for(var i = 0; i < manager.pendingRequests.length; i++){
            let pendingreq = document.createElement("ol");
            pendingreq.innerText = manager.pendingRequests[i].id + "  $" + manager.pendingRequests[i].amount + "  " + manager.pendingRequests[i].reason;
            document.getElementById("pending-requests").appendChild(pendingreq);
    }
    pendingViewed = true;
}



let requestDecision = document.getElementById("pending-requests-choice");
requestDecision.onclick = function(){
    let id = document.getElementById("request-id");
    if(id == null){
        document.getElementById("missing-id").hidden = false;
        return false;
    }else{
        for (let num of manager.pendingRequests){
            if(manager.pendingRequests.id == id){
                let yesChoice = document.getElementById("approve-btn").check;
                if(yesChoice){
                    let status = "APPROVED";
                    let choice = {id, status};
                    performAjaxPostRequest(employeeURL, JSON.stringify(choice), submissionSuccessful, submissionFailure);
                }else{
                    let status = "REJECTED";
                    let choice = {id, status};
                    performAjaxPostRequest(employeeURL, JSON.stringify(choice), submissionSuccessful, submissionFailure);
                }
            }else{
                document.getElementById("missing-id").hidden = false;
                return false;
            }
        }
    }
}

function submissionSuccessful(){
    document.getElementById("submit-failure").hidden = true;
    document.getElementById("submit-success").hidden = false;
    pendingViewed = false;
}

function submissionFailure(){
    document.getElementById("submit-success").hidden = true;
    document.getElementById("submit-failure").hidden = false;
}


let logOut = document.getElementById("log-out-btn");

logOut.onclick = function(){
    sessionStorage.removeItem("token");
    window.location.href = "Proj1HTML.html";

}