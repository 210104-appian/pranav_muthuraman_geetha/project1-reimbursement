const baseURL = "http://localhost:8080/EmployeeReimbursementApplication";

const loginURL = baseURL + "/login";

const employeeURL = baseURL + "/employees";

const managerURL = baseURL + "/managers";

// const testURL = baseURL + "/test";

function performAjaxGetRequest(url, callback){
    const xhr = new XMLHttpRequest();
    xhr.open("GET", url);
    xhr.onreadystatechange = function(){
        if(xhr.readyState == 4){
            if(xhr.status == 200){
                callback(xhr.response);
            }else{
                console.error("something went wrong with our GET request to " + url);
            }
        }
    }
    xhr.send();
}

function performAjaxPostRequest(url, payload, successCallback, failureCallback){
    const xhr = new XMLHttpRequest();
    xhr.open("POST", url);
    xhr.onreadystatechange = function(){
        if(xhr.readyState == 4){
            if(xhr.status > 199 && xhr.status < 300){
                successCallback(xhr.response);
            }
            else{
                if(failureCallback){
                    failureCallback();
                }else{
                    console.error("An error occured while attempting to create a new record")
                }                
            }
        }
    }
    xhr.send(payload);
}

