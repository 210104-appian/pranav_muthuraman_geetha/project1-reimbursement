let employee;
let pendingViewed = false;
let resolvedViewed = false;

window.onload = function(){
    employeeURI = employeeURL + "/" + sessionStorage.getItem("token");
    console.log("load has started");
    console.log(employeeURI);
    performAjaxGetRequest(employeeURI, loadEmployeeData)
}

function loadEmployeeData(responseText){
    console.log("load completed, check server console");
    console.log(responseText);
    employee = JSON.parse(responseText);
    console.log(employee);
    let welcome = document.createElement("h2");
    welcome.style = "text-align: center;"
    welcome.innerText = "Welcome " + employee.name;
    document.getElementById("welcome-msg").appendChild(welcome);
    return employee;
}

let PRequests = document.getElementById("pending-requests-btn");

PRequests.onclick = function(){
    if(pendingViewed == true){
        return false;
    }
    console.log(employee.id);
    let reqList = document.createElement("ul");
    let pendingHeader = document.createElement("ol");
    pendingHeader.innerText = "ID   Amount   Reason";
    document.getElementById("pending-requests").appendChild(reqList);
    document.getElementById("pending-requests").appendChild(pendingHeader);
    for(var i = 0; i < employee.requests.length; i++){
        if(employee.requests[i].status == "PENDING"){
            let pendingreq = document.createElement("ol");
            pendingreq.innerText = employee.requests[i].id + "  $" + employee.requests[i].amount + "  " + employee.requests[i].reason;
            document.getElementById("pending-requests").appendChild(pendingreq);
        }
    }
    pendingViewed = true;
}

let ResRequests = document.getElementById("resolved-requests-btn");

ResRequests.onclick = function(){
    if(resolvedViewed == true){
        return false;
    }
    let reqList = document.createElement("ul");
    let resolvedHeader = document.createElement("ol");
    resolvedHeader.innerText = "ID   Amount   Reason";
    document.getElementById("resolved-requests").appendChild(reqList);
    document.getElementById("resolved-requests").appendChild(resolvedHeader);
   
    for(let req of employee.requests){
        if(req.status == "APPROVED"){
            let resolvedreq = document.createElement("ol");
            resolvedreq.innerText = req.id + "   $" + req.amount + "   " + req.reason;
            document.getElementById("resolved-requests").appendChild(resolvedreq);
        }
    }
    resolvedViewed = true;
}

let requestSubmit = document.getElementById("request-submit-btn");

requestSubmit.onclick = function(){
    document.getElementById("submit-success").hidden = true;
    document.getElementById("submit-failure").hidden = true;
    let amount = document.getElementById("request-amt").value;
    let reason = document.getElementById("request-reason").value;
    if(amount == null || amount <= 0){
        document.getElementById("missing-amt").hidden = false;
        return false;
    }else if(reason == null || reason == ""){
        document.getElementById("missing-reason").hidden = false;
        return false;
    }
    let employeeID = employee.id;
    let newRequest = {amount, reason, employeeID};
    performAjaxPostRequest(employeeURL, JSON.stringify(newRequest),submissionSuccessful, submissionFailure);
}

function submissionSuccessful(){
    document.getElementById("submit-failure").hidden = true;
    document.getElementById("submit-success").hidden = false;
    pendingViewed = false;
}

function submissionFailure(){
    document.getElementById("submit-success").hidden = true;
    document.getElementById("submit-failure").hidden = false;
}

let logOut = document.getElementById("log-out-btn");

logOut.onclick = function(){
    sessionStorage.removeItem("token");
    window.location.href = "Proj1HTML.html";

}